'use strict';
var env = process.env;
var log = process.log;

var _ = require('lodash');

var userContext = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'userContext');

module.exports.addUsrPrcs1 = function(model, callback) {
    model.phn = model.cntryCd + model.phn;
    model.psCd = (env.NODE_ENV === 'production') ? _.random(1111, 9999).toString() : '1111' ;
    userContext.addUsrPrcs1(model, function(e, r){
        if(e){
            callback(e, null);
        } else{
            // SMS model.psCd to model.phn
            callback(null, {token: model.phn});
        }      
    });
};

module.exports.addUsrPrcs2 = function(model, callback) {
    userContext.addUsrPrcs2(model, callback);
};

module.exports.getUsr = function(model, callback) {
    console.log("bll")
    model.phn = model.cntryCd + model.phn;
    userContext.getUsr(model, callback);
};

module.exports.updateUsrPhnPrcs1 = function(model, callback) {
    model.phn = model.cntryCd + model.phn;
    model.nwPhn = model.nwCntryCd + model.nwPhn;   
    model.nwPsCd = (env.NODE_ENV === 'production') ? _.random(1111, 9999).toString() : '1111';
    userContext.updateUsrPhnPrcs1(model, function(e, r){
        if(e){
            callback(e, null);
        } else{
            // SMS r.psCd to model.phn and r.nwPsCd to model.nwPhn
            callback(null, {token: model.phn});
        }      
    });
};

module.exports.updateUsrPhnPrcs2 = function(model, callback) {
    userContext.updateUsrPhnPrcs2(model, callback);
};

module.exports.updateUsrPswrdPrcs1 = function(model, callback) {
    model.phn = model.cntryCd + model.phn;
    userContext.updateUsrPswrdPrcs1(model, function(e, r){
        if(e){
            callback(e, null);
        } else{
            // SMS r.psCd to model.phn
            callback(null, {token: model.phn});
        }      
    });
};

module.exports.updateUsrPswrdPrcs2 = function(model, callback) {
    userContext.updateUsrPswrdPrcs2(model, callback);
};

module.exports.getCrcl = function(model, callback) {
    userContext.getCrcl(model, callback);
};

module.exports.getUsrNtfctn = function(model, callback) {
    userContext.getUsrNtfctn(model, callback);
};


