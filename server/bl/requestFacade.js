'use strict';
var env = process.env;
var log = process.log;

var requestContext = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'requestContext');
var validationUtil = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

module.exports.addRqstPrcs1 = function(model, callback) {
    requestContext.addRqstPrcs1(model,callback);

};

module.exports.updateRqstPrcs2 = function(model, callback) {
    requestContext.updateRqstPrcs2(model,callback);    
};

module.exports.addAllRqstPrcs1 = function(model, callback) {
    requestContext.addAllRqstPrcs1(model,callback);    
};

module.exports.getRqst = function(model, callback) {
    requestContext.getRqst(model,callback);    
};

module.exports.addArmRqstPrcs1 = function(model, callback) {
    requestContext.addArmRqstPrcs1(model,callback);    
};

