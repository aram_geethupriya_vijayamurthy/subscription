'use strict'
var env = process.env;
var log = process.log;
var validator = require('validator');
var restify = require('restify');


module.exports = {
    sbsc: {
        addSbsc: function(model, validCB, cb) {
         
            if(validateEmail(model.eml))  cb(new restify.BadRequestError(ERR_INVALID_EMAIL), null) ;
            if (validateUsrNm(model.usrnm)) cb(new restify.BadRequestError(ERR_INVALID_USRNM), null);
            if (validateUsrNm(model.prmCd)) cb(new restify.BadRequestError(ERR_INVALID_PRMCD), null) ;
            else validCB(model, cb);


        },

        getEmail: function(model, validCB, cb) {

              (validateEmail(model.eml)) ? cb(new restify.BadRequestError(ERR_INVALID_EMAIL), null) : validCB(model, cb);
        },



      },

        ERR_INVALID_EMAIL: 'ERR_INVALID_EMAIL',
        ERR_INVALID_USRNM: 'ERR_INVALID_USRNM',
        ERR_INVALID_PRMCD: 'ERR_INVALID_PRMCD',
        ERR_USRNM_EXIST: 'ERR_USRNM_EXIST',
        validateEmail : validateEmail,
        validateUsrNm  : validateUsrNm  
}

   var validateEmail =  function(eml) {
        if (validator.isNull(eml) || !validator.isEmail(eml)) 
            return true;
        
    }

    var validateUsrNm = function(usrNm) {
        if ((validator.isNull(usrNm) || !validator.isAlphanumeric(usrNm))) {
             return true;
        } else if (validator.matches(usrNm, '^([A-Za-z]|[0-9]|_)+$')) {
             return false;
    }
}



