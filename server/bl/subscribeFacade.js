'use strict';
var env = process.env;
var log = process.log;

var subscribeContext = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'subscribeContext');

module.exports.addSbscrb = function(model, callback) {
    model.typ = 'invite';
    if (!model.prmCd) model.prmCd = 'aram';
    if (!model.lcl) model.lcl = 'en_US';
    subscribeContext.addSbscrb(model, callback);
};

module.exports.getSbscrbEml = function(model, callback) {
    subscribeContext.getSbscrbEml(model, callback);
};
