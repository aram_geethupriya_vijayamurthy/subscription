'use strict';
var env = process.env;
var log = process.log;

var connectionContext = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'connectionContext');
var validationUtil = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');


module.exports.getCntn = function(model, callback) {
    console.log("facade")
    connectionContext.getCntn(model,callback);

};

module.exports.removeCntn = function(model, callback) {
    console.log("facade")
    connectionContext.removeCntn(model,callback);

};