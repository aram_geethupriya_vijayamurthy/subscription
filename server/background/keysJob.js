'use strict'

require('dotenv').config({ path: './server/.env' });
var path = require('path');
process.env.ROOT = path.join(__dirname, '..', '..');
process.env.SLASH = path.sep;
var env = process.env;

var ln = require("ln");
var log = process.log = new ln({
    "name": env.BJ_KEYS_LOG_NAME,
    "level": env.BJ_KEYS_LOG_LEVEL,
    "appenders": [{
        "type": "file",
        "path": env.BJ_KEYS_LOG_PATH
    }]
});

var cp = require('child_process');
var async = require('async');
var _ = require('lodash');

var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var total = 0, completed = 0;


var retQKeyGenCB = function() {
    return function qKeyGenCB(e, r) {
        if (e) {
            log.e("error");
        } else {
            db.execute(db.queries.sbscrb.update, r, function updateSbscKeyCB(dummy, res1) {
                ++completed;
                if (total == completed)
                    process.exit();
            });
        }
    }
};

var q = async.queue(function qKeyGen(eml, callback) {
    var pvtKyCP = cp.exec('openssl genrsa 3072 ', function(e, stdout, stderr) {
        if (e) {
            log.error(e)
            callback(e, null);
        } else {
            var prvtKy = stdout,
                pbKyCP = cp.spawn('openssl', ['rsa', '-pubout'])
            pbKyCP.stdout.setEncoding('ascii');
            pbKyCP.stdin.write(prvtKy);
            pbKyCP.stdin.end();
            pbKyCP.stdout.on('data', function(data) {
                var model = {
                    eml: eml,
                    prvtKy: prvtKy,
                    pblcKy: data
                };
                callback(null, model);
            });
        }
    });
}, _.toInteger(env.BJ_KEYS_PARALLEL));

//q.drain = function() {
//    console.log('all items have been processed');
//};

db.executeRow(db.queries.sbscrb.getKyMV, { max: _.toInteger(env.BJ_KEYS_MAX) }, function getSbscEmlCB(dummy, row) {
    q.push(row.eml, retQKeyGenCB());
}, function(err, result) {
    if (err) {
        log.e(err);
        process.exit(1);
    } else {
        if (result.rowLength < 1)
            process.exit();
        total = result.rowLength;
    }
});