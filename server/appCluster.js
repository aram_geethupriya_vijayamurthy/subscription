'use strict';

if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
}

var path = require('path');
process.env.ROOT = path.join(__dirname, '..');
process.env.SLASH = path.sep;
require('dotenv').config({path: './server/.env'});
var env = process.env;
var ln = require("ln");
var log = process.log = new ln({
        "name": env.LOG_NAME,
        "level": env.LOG_LEVEL,
        "appenders": [{
          "type": "file",
          "path": env.LOG_PATH
        }]
      });

function startAServer(){
    var server = require('./appServer');
    server.listen(env.SERVER_PORT, env.SERVER_IP, function onServerStart() {
        log.warn(env.SERVER_NAME + ' is now listening on ' + server.url + ' using pid# ' + process.pid);
    });
}

try{
    var cluster = require('cluster');
    if(cluster.isMaster){
        require('./appDDL').prepareDB(function prepareDBCallback(e, r){
            if(e){
                log.error(e);
            }else if(env.NODE_ENV == 'production'){
                var numCPUs = require('os').cpus().length;
                log.info('Starting master pid#' + process.pid + '. Spawning ' + numCPUs + ' workers.');
                for (var i = 0; i < numCPUs; i++) {
                    cluster.fork();
                }
                cluster.on('exit', function onClusterExit(worker, code, signal) {
                    log.warn('worker pid#' + worker.process.pid + ' died. Code >> ' + code + '. Signal >> ' + signal);
                    cluster.fork();
                });
            }else{
                startAServer();
            }
        });
    }else{
        log.info('Starting '+ env.NODE_ENV +' worker pid#' + process.pid + ' for ' + env.SERVER_IP + ':' + env.SERVER_PORT);
        startAServer();
    } 
}catch(e){
    log.error(e);
}
