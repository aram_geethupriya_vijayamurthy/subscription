'use strict'
require('dotenv').config({ path: './server/.env' });
var env = process.env;
var ln = require("ln");
var log = process.log = new ln({
    "name": env.LOG_NAME,
    "level": env.LOG_LEVEL,
    "appenders": [{
        "type": "file",
        "path": env.LOG_PATH
    }]
});
var async = require('async');
var _ = require('lodash');
var cp = require('child_process')
    , assert = require('assert')
    ;

var db = require('C:\\nodejs\\aram_project\\aram_life\\server\\dal\\helper\\databaseHelper');
var createTaskCB = function() {
    return function taskCB(e, r) {
        if (e) {
            console.log("error");
        } else {
            log.i('key generated successfully for this usr' + r);
            db.execute(db.queries.user.addKey, r, function addCallback(dummy, res1) {
                console.log("done" + JSON.stringify(dummy));
            });


        }
    }
};
//as soon as que push
var q = async.queue(function(usr, callback) {
    console.log("que")
    var pvtKyCP = cp.exec('openssl genrsa 3072', function(e, stdout, stderr) {
        if (e) {
            console.log("Erro")
            callback(e, null);
        } else {
            var prvtKy = stdout, pbKy,
                pbKyCP = cp.spawn('openssl', ['rsa', '-pubout'])
            pbKyCP.stdout.setEncoding('ascii');
            pbKyCP.stdin.write(prvtKy);
            pbKyCP.stdin.end();
            pbKyCP.stdout.on('data', function(data) {
                pbKy = data;
                var model = {
                    eml: usr,
                    prvtKy: prvtKy,
                    pblcKy: pbKy,
                    kyFlg: true
                }

                callback(null, model);
            });
       }
    });
}, 2);

q.drain = function() {
    console.log('all items have been processed');
}

//first

db.executeRow(db.queries.user.getEmails, {}, function emailCallback(dummy, res) {
    console.log("res" + JSON.stringify(res));
    var usr = res.eml;
    console.log("usr" + usr)
    q.push(usr, createTaskCB());
}, function(err, result) {

    console.log("All emails processed")
}

);
