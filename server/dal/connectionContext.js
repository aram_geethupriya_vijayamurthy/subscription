'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var async = require('async');
var _ = require('lodash');

var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var q = db.queries;
var e = db.execute;
var b = db.executeBatch;
module.exports.getCntn = function(model, callback) {
    console.log("conetct")
    e(q.cntn.get, model, callback, function(d, r1) {
        if (r1.rowLength > 0) {
            callback(null, r1.rows);
        } else {
            callback(['noconnection'], null)
        }

    });
};

module.exports.removeCntn = function(model, callback) {
    console.log("conetct")
    var queries = [];
    var frndModel1 = _.cloneDeep(model);
    var frndModel2 = {};
    frndModel2.usrId = model.frndId;
    frndModel2.frndId = model.usrId;
    
    queries.push({
        query: q.cntn.remove,
        params: frndModel1

    });
    queries.push({
        query: q.cntn.removeMV,
        params: frndModel1

    });

    queries.push({
        query: q.cntn.remove,
        params: frndModel2

    });
    queries.push({
        query: q.cntn.removeMV,
        params: frndModel2

    });
    b(queries, callback);
};
