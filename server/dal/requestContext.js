'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var async = require('async');
var _ = require('lodash');

var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var cryptoUtil = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'cryptoUtil');
var q = db.queries;
var e = db.execute;
var b = db.executeBatch;

module.exports.addRqstPrcs1 = function(model, callback) {
    console.log("context")
    var dtls = JSON.parse(model.dtlsJSON);
    var usrMap = [];
    usrMap.push(model.usrId);
    usrMap.push(model.frndId);
    e(q.usr.getAll, { id: usrMap }, callback, function(d2, r) {
        if (r.rowLength == usrMap.length) {
            //validate dtls JSON for valid usrId
            if (dtls[model.usrId] && dtls[model.frndId]) {
                //check if an open rqst already exist
                e(q.rqst.getRqst, { usrId: model.frndId, frmId: model.usrId, typ: env.USR_REQSTFOR_CNTN }, callback, function(d, r1) {
                    e(q.rqst.getRqst, { usrId: model.usrId, frmId: model.frndId, typ: env.USR_REQSTFOR_CNTN }, callback, function(d1, r2) {
                        if (r1.rowLength > 0 || r2.rowLength > 0) {
                            callback(['InvldRqst'], null)
                        }
                        else {
                            //new reqst, then create an entry in rqst
                            model.id = db.getNewId();
                            var dtls = JSON.parse(model.dtlsJSON);
                            _.forEach(r.rows, function(usr) {
                                var usr1 = _.pick(usr, ['id', 'flNm', 'usrnm', 'arms', 'rnk', 'pblKy', 'prflPc']);
                                dtls[usr.id] = _.assign(dtls[usr.id], usr1);
                            })

                            var frmModel = {}
                            frmModel.usrId = model.usrId;
                            frmModel.dtlsJSON = JSON.stringify(dtls);
                            frmModel.frmId = model.usrId;
                            frmModel.typ = env.USR_REQSTFOR_CNTN;
                            frmModel.id = model.id;

                            var queries = [];
                            queries.push({
                                query: q.rqst.add,
                                params: frmModel

                            });

                            var toModel = {}
                            toModel.usrId = model.frndId;
                            toModel.dtlsJSON = JSON.stringify(dtls);
                            toModel.frmId = model.usrId;
                            toModel.typ = env.USR_REQSTFOR_CNTN;
                            toModel.id = model.id;


                            queries.push({
                                query: q.rqst.add,
                                params: toModel

                            });

                            //frm circl friends, usr adds friend. remove old cirlc req frm rqst
                            if (model.rqstId) {
                                queries.push({
                                    query: q.rqst.remove,
                                    params: { id: model.rqstId }

                                });
                            }
                            b(queries, callback);
                        }
                    });
                });

            } else {
                //dtlsJSON has invalid userId\
                console.log("invalid user in json");
                callback(['InvldUsrJSON'], null)
            }
        } else {
            //invalid user
            console.log("invalid user in request");
            callback(['InvldUsrRqst'], null)
        }
    });
};

module.exports.updateRqstPrcs2 = function(model, callback) {
    var queries = [];
    var dtls = JSON.parse(model.dtlsJSON);
    console.log("dtls" + dtls)
    console.log("userId" + model.usrId)
    var usrMap = [];
    e(q.rqst.get, model, callback, function(d0, r0) {
        if (r0.rowLength > 0) {
            _.forEach(r0.rows, function(rqst) {
                console.log(JSON.stringify(rqst))
                if (dtls[rqst.usrId]) {
                    usrMap.push(rqst.usrId)
                }
                else
                    callback(['InvldUsrRqst'], null)
            });
            console.log('usrMap::: ' + JSON.stringify(usrMap));
            var status = dtls[model.usrId].status;
            console.log("status" + status)
            if (status == 'A') {
                var crtrId = r0.rows[0].frmId;
                //get all participants details
                e(q.usr.getAll, { id: usrMap }, callback, function(d1, r3) {
                    //loop through all participants
                    console.log("r33" + JSON.stringify(r3));
                    
                    var frnd = _.find(r3.rows, function(i) {
                            return i.id == model.usrId;
                        })
                    console.log('frnd: ' + JSON.stringify(frnd ))
                    
                    var crtr = _.find(r3.rows, function(j) {
                            return j.id == crtrId;
                        })    

                    console.log('crtr: ' + JSON.stringify(crtr ))
                    console.log('frnd after: ' + JSON.stringify(frnd ))
                    
                    
                    var frndModel = crtr;
                    frndModel.usrId = model.usrId;   
                    frndModel.encShrdKy = dtls[model.usrId].encShrdKy;                    
                    frndModel.frndId = crtrId;
                    
                    var crtrModel = frnd;
                    crtrModel.usrId = crtrId;   
                    crtrModel.encShrdKy = dtls[crtrId].encShrdKy;                    
                    crtrModel.frndId = model.usrId;
                    
                    
                    console.log('frndModel: ' + JSON.stringify(frndModel ))
                    console.log('crtModel: ' + JSON.stringify(crtrModel ))
                    
                    
                     queries.push({
                        query: q.cntn.add,
                        params: frndModel

                    });
                    queries.push({
                        query: q.cntn.addMV,
                        params: frndModel

                    });
                    
                        queries.push({
                        query: q.cntn.add,
                        params: crtrModel

                    });
                    queries.push({
                        query: q.cntn.addMV,
                        params: crtrModel

                    });
                    
                    console.log("ftt queries" + JSON.stringify(queries))

                    b(queries, callback, function(d2, r5) {
                        if (d2 == null) {
                            e(q.rqst.remove, model, callback, callback);
                        }
                    });

                });
            } else {
                e(q.rqst.remove, model, callback, callback);
            }
        }
        else {
            callback(['InvldUsrRqst'], null)
        }



    });
}

module.exports.getRqst = function(model, callback) {
    console.log("model" + JSON.stringify(model))
    e(q.rqst.getAll, model, callback, function(d5, r5) {
        console.log("d5" + JSON.stringify(d5))
        console.log("r5" + JSON.stringify(r5))
        if (r5.rowLength > 0) {
            var i = 0;
            _.forEach(r5.rows, function(rqst) {
                var dtls = rqst.dtlsJSON;
                var crtrId = rqst.frmId;
                if (dtls.users) {
                    for (var i in dtls.users) {
                        if (i != rqst.frmId && i != crtrId) {
                            dtls.users = _.omit(dtls.users, i);
                        }
                    }
                    r5.rows[i].dtlsJSON = dtls;
                    i++;

                }

            })
        }
        callback(null, r5.rows);
    });
}

module.exports.addArmRqstPrcs1 = function(model, callback) {

    var armRqstId = '';
    //now validate detail JSON
    var dtlsJSON = JSON.parse(model.dtlsJSON);
    var usrIds = dtlsJSON.users;
    var mnArm = dtlsJSON.mnArm;
    var usrMap = [];
    var usrFrndMap = [];
    var emlMap = [];
    var rcvrCntr = 0;
    //prepare data for validation
    for (var i in usrIds) {
        var usrId = dtlsJSON.users[i];
       
        if ((/\w{8}(-\w{4}){3}-\w{12}?/).test(i)) {
            console.log("insode")
            //contains all users involved in arm
            usrMap.push(i);
            if (usrId.frnd == 'F') {
                //contains friends of creator
                usrFrndMap.push(i);
            }
        }
        else
            if ((/^([\w!.%+\-])+@([\w\-])+(?:\.[\w\-]+)+$/).test(i))
                //contains non member involved in aram
                emlMap.push(i)
        //check receiver        
        if (_.isEqual(usrId.type, 'R')) {
            rcvrCntr++;
        }


    }
    console.log("usrMap" + JSON.stringify(usrMap))
    console.log("usrFrndMap" + JSON.stringify(usrFrndMap))
    console.log("rcvrCntr" + JSON.stringify(rcvrCntr))
    e(q.usr.getAll, { id: usrMap }, callback, function(d2, r2) {
        if (r2.rowLength > 0) {
            if (r2.rowLength == usrMap.length) {
                //valid userId in JSON
                //Then check valid Min arams in JSOn
                if (rcvrCntr == mnArm) {
                    //check whether the user is a friend with other users.get all friend list of creator
                    console.log("model.usrId" + model.usrId)
                    e(q.cntn.getUsrId, { usrId: model.usrId }, callback, function(d3, r3) {
                        var frndMap = [];
                        var queries = [];
                        console.log("r3:::" + JSON.stringify(r3))
                        if (r3.rowLength > 0) {
                            frndMap = r3.rows;
                            var isFriend = "false";
                            _.forEach(usrFrndMap, function(UsrFrndId) {
                                console.log("UsrFrndId" + UsrFrndId)
                                if (_.indexOf(frndMap, UsrFrndId) == -1) {
                                    console.log("insode call bck")
                                    isFriend = "true";

                                }

                            })
                            if (isFriend == 'true') {
                                callback(["InvalidFrnd"], null)
                            }
                        }
                        //if all the users are valid
                        armRqstId = db.getNewId();

                        var crtrDtls = _.find(r2.rows, function(i) {
                            return i.id == model.usrId

                        })

                        _.forEach(usrMap, function(armUsrId) {
                            console.log("insode arm")
                            e(q.usr.get, { id: armUsrId }, callback, function(d5, r5) {
                                if (r5.rowLength > 0) {

                                    if (model.usrId == armUsrId) {
                                        crtrDtls = r5.rows[0];
                                        //check for valid aram ig creator is a giver
                                        if (usrIds[model.usrId].type == 'G') {
                                            if (usrIds[model.usrId].aram > crtrDtls.arms) {
                                                callback("InvalidArm", null)
                                            }
                                        }
                                    }

                                    var armModel = {}
                                    armModel.id = armRqstId;
                                    armModel.usrId = armUsrId;
                                    armModel.frmId = model.usrId;
                                    armModel.typ = '';
                                    armModel.dtlsJSON = model.dtlsJSON;
                                    armModel.dtlsJSON[model.usrId].flNm = crtrDtls.flNm;
                                    armModel.dtlsJSON[model.usrId].flNm = crtrDtls.flNm;
                                    queries.push({
                                        query: q.rqst.add,
                                        params: armModel

                                    });



                                }

                            });







                        })
                        //add emlRqst
                        _.forEach(emlMap, function(emlId) {
                            var emlModel = {};
                            emlModel.id = armRqstId;
                            emlModel.eml = emlId;
                            emlModel.typ = '';
                            emlModel.dtlsJSON = model.dtlsJSON;
                            queries.push({
                                query: q.arm.addEml,
                                params: emlModel

                            });
                            queries.push({
                                query: q.arm.addEmlMV,
                                params: emlModel

                            });

                        })

                        b(queries, callback);

                    });

                } else {
                    //Invalid Minimum aram
                    callback("InVLIDJSON", null);
                }

            } else {
                //invalid JSON. NOt all usr Id in JSON is valid
                callback("InVLIDJSON", null);
            }

        }
    });



}








