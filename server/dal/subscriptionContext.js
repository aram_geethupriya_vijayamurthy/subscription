'use strict';
var env = process.env;
var log = process.log;
var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var restify = require('restify');
var validatorHelper = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'helper'  + env.SLASH + 'validationHelper');

module.exports.addSbsc = function(model, callback) {
    var queries =[];
     model.kyFlg = false;
    model.lmTm = db.getNewId();
    //to check if username is available
    db.execute(db.queries.user.getSbscUsrEml, model, callback, function getSbscUsrEmlCB(dummy, res) {
        if (res.rowLength > 0) {
             callback(null, res.rows[0]);
        } else {
            db.execute(db.queries.user.getSbsc, model, callback, function getSbscCB(dummy, res1) {
                if (res1.rowLength > 0) {
                     
                    callback(new restify.BadRequestError(validatorHelper.ERR_USRNM_EXIST), null);
                } else {
                    queries.push({
                        query : db.queries.user.addSbsc,
                        params : model
                    });
                    
                     model.typ = 'invite';
                     
                      queries.push({
                        query : db.queries.user.addPrm,
                        params : model
                    });
                    console.log("queries"+JSON.stringify(queries))
                    db.executeBatch(queries, callback,callback);
              
              }

            });
        }

    });

};


module.exports.getEmail = function(model, callback) {
    db.execute(db.queries.user.getSbscUsrEml, model, callback, function getSbscUsrEmlCB(dummy, res) {
        if (res.rowLength > 0) {
            callback(null, res.rows[0]);
        } else {
            callback(null,{});

        }
    });

};