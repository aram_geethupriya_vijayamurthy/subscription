'use strict';
var env = process.env;
var log = process.log;

var _ = require('lodash');
var cassandra = require('cassandra-driver');
var TimeUuid = require('cassandra-driver').types.TimeUuid;
var Long = require('cassandra-driver').types.Long;


module.exports = {
    getNewId: function() {
        return TimeUuid.now();
    },
    client: function() {
        if (true || !process.dbClient) {
            var options = {
                contactPoints: env.DB_CONTACT_POINTS.split(','),
                keyspace: env.DB_KEYSPACE
            };
            if (!_.isEmpty(env.DB_PORT)) {
                options.protocolOptions = {
                    port: env.DB_PORT
                };
            }
            if (!_.isEmpty(env.DB_USERNAME)) {
                options.authProvider = new cassandra.auth.PlainTextAuthProvider(env.DB_USERNAME, env.DB_PASSWORD);
            }

            var client = new cassandra.Client(options);

            client.on('log', function(level, className, message, furtherInfo) {
                if (level == 'info' || level == 'verbose')
                    log.info('log event: %s -- %s', level, message);
                if (level == 'warning')
                    log.warn('log event: %s -- %s', level, message);
                if (level == 'error')
                    log.error('log event: %s -- %s', level, message);
            });

            process.dbClient = client;
        }
        return process.dbClient;
    },
    execute: function(query, model, callback, resultCallback) {
        console.log("query"+JSON.stringify(query));
        
        resultCallback = resultCallback || callback;
        module.exports.client().execute(query, model, { prepare: true }, function(err, result) {
            console.log("error"+JSON.stringify(err))
           if (err) {
                callback(err, null);
            } else {
                console.log("else")
                if (result.rows){
                    console.log("ifff")
                    result.rows = module.exports.cleanResult(result.rows);
                }
                resultCallback(null, result);
            }
        });
    },
    executeBatch: function(queries, callback, resultCallback, counter) {
        resultCallback = resultCallback || callback;
        module.exports.client().batch(queries, { prepare: true, counter: counter || false }, function(err) {
          
            if (err) {
                  console.log("err"+JSON.stringify(err))
                callback(err, null);
            } else {
                  console.log("else")
                resultCallback(null);
            }
        });
    },
    executeRow: function(query, model, rowCallback, callback, resultCallback) {
        resultCallback = resultCallback || callback;
        module.exports.client().eachRow(query, model, { prepare: true }, function(n, row) {
            rowCallback(n, row);
        }, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                if (result.rows)
                    result.rows = module.exports.cleanResult(result.rows);
                resultCallback(null, result);
            }
        });
    },
    cleanResult: function(rows) {
      
        rows = _.transform(rows, function(r1, n1, k1) {
            r1[k1] = _.transform(n1, function(r2, n2, k2) {
                if (_.endsWith(k2, 'JSON')) {
                    k2 = _.replace(k2, 'JSON', '');
                    r2[k2] = JSON.parse(n2);
                } else {
                    r2[k2] = n2 instanceof Long || n2 instanceof TimeUuid ? n2.toString() : n2;
                }
            });
        });
        return rows;
        //return _.cloneDeep(rows);
    },
    queries: {
        sbscrb: {
            add: 'INSERT INTO "sbscrb" ("usrnm", "eml", "kyFlg", "prmCd", "lmTm", "lcl", "rtUsrnm") VALUES (:usrnm, :eml, false, :prmCd, now(), :lcl , :rtUsrnm);',
            get: 'SELECT "usrnm", "eml" FROM "sbscrb" WHERE "eml" = :eml;',
            getAll: 'SELECT * FROM "sbscrb" WHERE "eml" = :eml;',
            update: 'UPDATE "sbscrb" SET "prvtKy" = :prvtKy, "pblcKy" = :pblcKy, "kyFlg" = true WHERE eml=:eml;',
            getUsrMV: 'SELECT * FROM "sbscrbUsrMV" WHERE "usrnm" = :usrnm;',
            
            getKyMV: 'SELECT "eml" FROM "sbscrbKyMV" WHERE "kyFlg" = false LIMIT :max ;',
            removeKy: 'UPDATE "sbscrb" SET "prvtKy" = null, "pblcKy" = null WHERE eml=:eml;'
        },
        prm: {
            add: 'INSERT INTO "prm" ("cd", "typ") VALUES (:prmCd, :typ);'
        },
        usrByPhn: {
            add: 'INSERT INTO "usrByPhn" ("id", "cntryCd", "encPhn", "psCd", "eml", "flNm", "encPswrd") VALUES (:id, :cntryCd, :encPhn, :psCd, :eml, :flNm, :encPswrd);',
            get: 'SELECT * FROM "usrByPhn" WHERE "encPhn" = :encPhn;',
            updateEncPswrd: 'UPDATE "usrByPhn" SET "encPswrd" = :encPswrd WHERE "encPhn" = :encPhn;',
            updateEncNwPhn: 'UPDATE "usrByPhn" SET "nwCntryCd" = :nwCntryCd, "encNwPhn" = :encNwPhn, "nwPsCd" = :nwPsCd WHERE "encPhn" = :encPhn;',
            remove: 'DELETE FROM "usrByPhn" WHERE "encPhn" = :encPhn;'
        },
        usr: {
            add: 'INSERT INTO "usr" ("id", "usrnm", "flNm", "prflPc", "rnk", "arms", "eml", "cntryCd", "encPrvtKy", "pblcKy", "encShrdKy", "prmCd", "llTm", "lmTm","rtUsrnm") VALUES (:id, :usrnm, :flNm, :prflPc, \'5\', \'0\', :eml, :cntryCd, :encPrvtKy, :pblcKy, :encShrdKy ,:prmCd, now(), now(),:rtUsrnm);',
            get: 'SELECT * FROM "usr" WHERE "id" = :id;',
            getAll: 'SELECT * FROM "usr" WHERE "id" in :id;',
            getUsrCrcl: 'SELECT * FROM "usrCrclMV" WHERE "rtUsrnm" = :rtUsrnm;', 
            updateCntryCd: 'UPDATE "usr" SET "cntryCd" = :cntryCd, "lmTm" = now() WHERE "id" = :id;',
            updateLlTm: 'UPDATE "usr" SET "llTm" = now() WHERE "id" = :id;'
        },
         rqst: {
            add: 'INSERT INTO "rqst" ("id", "usrId", "frmId",  "typ","dtlsJSON") VALUES (:id, :usrId, :frmId,  :typ, :dtlsJSON);',
            get : 'SELECT  * FROM "rqst" where "id" = :id;',
            remove: 'DELETE FROM "rqst" WHERE "id" = :id;',
            getAll: 'SELECT * FROM "rqstMV" where "usrId" = :usrId ;',
            getRqst:'SELECT * FROM "rqstMV" WHERE "usrId" = :usrId AND "frmId" = :frmId AND "typ" = :typ'
           
         },
         cntn: {
              add: 'INSERT INTO "cntn" ("usrId", "frndId", "usrnm",	"flNm",	"prflPc", "rnk", "arms", "pblcKy", "encShrdKy") VALUES(:usrId, :frndId, :usrnm, :flNm, :prflPc, :rnk, :arms, :pblcKy, :encShrdKy)',
              addMV: 'INSERT INTO "cntnMV" ("usrId", "frndId", "usrnm",	"flNm",	"prflPc", "rnk", "arms", "pblcKy", "encShrdKy") VALUES(:usrId, :frndId, :usrnm, :flNm, :prflPc, :rnk, :arms, :pblcKy, :encShrdKy)',
              get: 'SELECT * FROM "cntnMV" WHERE "usrId" = :usrId;',
              remove: 'DELETE FROM "cntn" WHERE "usrId" = :usrId AND "frndId" = :frndId',
              removeMV: 'DELETE FROM "cntnMV" WHERE "usrId" = :usrId AND "frndId" = :frndId',
              getUsrId: 'SELECT "frndId" FROM "cntnMV" WHERE "usrId" = :usrId;'
         },
         
        ntfctn: {
            add: 'INSERT INTO "ntfctn" ("usrId", "id", "sbjctJSON", "vrb", "objctJSON") VALUES (:usrId, now(), :sbjctJSON, :vrb, :objctJSON);',
            get: 'SELECT unixTimestampOf("id") AS "timestamp", "sbjctJSON", "vrb", "objctJSON" FROM "ntfctn" WHERE "usrId" = :usrId;'

    },
    arm: {
        add:    'INSERT INTO "emlRqst"("id", "eml", "typ",	"dtlsJSON") VALUES (:id, :eml, :typ, :dtlsJSON);',
        addMV:  'INSERT INTO "emlRqstMV"("id", "eml", "typ",	"dtlsJSON") VALUES (:id, :eml, :typ, :dtlsJSON);'
        
    }
    }
};