'use strict';
var env = process.env;
var log = process.log;

var _ = require('lodash');
var bcrypt = require('bcryptjs');

var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var cryptoUtil = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'cryptoUtil');

var q = db.queries;
var e = db.execute;
var eB = db.executeBatch;

module.exports.addUsrPrcs1 = function(model, callback) {
    model.id = db.getNewId();
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    model.encPswrd = bcrypt.hashSync(model.pswrd);
    e(q.usrByPhn.add, model, callback);
};

module.exports.addUsrPrcs2 = function(model, callback) {

    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    //check if phn num exist
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        var usrByPhn = r1.rows[0];
        if (r1.rowLength > 0 && usrByPhn.psCd == model.psCd) {
            e(q.sbscrb.getAll, { eml: usrByPhn.eml }, callback, function(d, r2) {
                var sbscrb = r2.rows[0];
                if (r2.rowLength > 0) {
                    var usrModel = _.assign(sbscrb, usrByPhn);
                    usrModel.prflPc = '//www.gravatar.com/avatar/' + cryptoUtil.md5(usrModel.eml) + '.jpg';
                    //             usrModel.encPrvtKy = cryptoUtil.aesEnc(model.phn, usrModel.prvtKy);
                    //  usrModel.encShrdKy = cryptoUtil.pblcEnc(usrModel.pblcKy, cryptoUtil.gnKy());
                    usrModel.encPrvtKy = 'asas';
                    usrModel.encShrdKy = 'bsb';
                    var queries = [];
                    var qs = [
                        {
                            query: q.usr.add,
                            params: usrModel
                        },
                        {
                            query: q.sbscrb.removeKy,
                            params: usrModel
                        }
                    ];

                    eB(qs, callback, function(d3, r3) {
                        if (d3 == null) {
                            e(q.usr.getUsrCrcl, usrModel, callback, function(d4, r4) {
                                var queries1 = [];
                                if (r4.rowLength > 0) {


                                    _.forEach(r4.rows, function(crcl) {
                                        var rqstModel = {};

                                        if (crcl.id != usrModel.id) {
                                            //to get request creator details
                                            var dtlsJSON = {};
                                            dtlsJSON[crcl.id] = crcl;

                                            // for the signed in user add request
                                            var rqstUsrModel = {};
                                            rqstUsrModel.id = db.getNewId();
                                            rqstUsrModel.usrId = usrModel.id;
                                            rqstUsrModel.frmId = usrModel.id;
                                            rqstUsrModel.typ = env.ARM_REQSTFOR_CNTN;
                                            rqstUsrModel.dtlsJSON = JSON.stringify(dtlsJSON)
                                            queries1.push({
                                                query: q.rqst.add,
                                                params: rqstUsrModel

                                            });


                                        }

                                    })
                                    if (queries1.length > 0) {
                                        eB(queries1, callback, function(d5, r5) {
                                            callback(null, { token: usrModel.id });
                                        });
                                    } else {
                                        callback(null, { token: usrModel.id });
                                    }

                                }
                                else {
                                    callback(null, { token: usrModel.id });
                                }
                            });

                        }
                        else {
                            callback(['error'], null)
                        }

                    });

                } else {
                    callback(['sbscrbNtExst'], null)
                }
            });
        } else {
            callback(['psCdWrng'], null)
        }
    });
};

module.exports.getUsr = function(model, callback) {
    console.log("conetct")
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        var usrByPhn = r1.rows[0];
        console.log("usrByPhn" + usrByPhn)
        if (r1.rowLength > 0 && bcrypt.compareSync(model.pswrd, usrByPhn.encPswrd)) {
            e(q.usr.get, usrByPhn, callback, function(d, r2) {
                var usr = r2.rows[0];
                usr.token = usr.id;
                e(q.usr.updateLlTm, usr, callback, function(d, r3) {
                    callback(null, usr);
                });
            });
        } else {
            callback(['pswrdWrng'], null)
        }
    });
};

module.exports.updateUsrPhnPrcs1 = function(model, callback) {
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    model.encNwPhn = bcrypt.hashSync(model.nwPhn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        var usrByPhn = r1.rows[0];
        if (r1.rowLength > 0 && bcrypt.compareSync(model.pswrd, usrByPhn.encPswrd)) {
            e(q.usrByPhn.get, { encPhn: model.encNwPhn }, callback, function(d, r2) {
                if (r2.rowLength > 0) {
                    callback(['phnExst'], null);
                } else {
                    e(q.usrByPhn.updateEncNwPhn, model, callback);
                }
            });
        } else {
            callback(['pswrdWrng'], null);
        }
    });
};

module.exports.updateUsrPhnPrcs2 = function(model, callback) {
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        var usrByPhn = r1.rows[0];
        if (r1.rowLength > 0 && usrByPhn.psCd == model.psCd && usrByPhn.nwPsCd == model.nwPsCd) {
            usrByPhn.encPhn = usrByPhn.encNwPhn;
            usrByPhn.cntryCd = usrByPhn.nwCntryCd;
            var qs = [
                {
                    query: q.usrByPhn.remove,
                    params: model
                },
                {
                    query: q.usrByPhn.add,
                    params: usrByPhn
                },
                {
                    query: q.usr.updateCntryCd,
                    params: usrByPhn
                }
            ];

            eB(qs, callback);
        } else {
            callback(['psCdWrng'], null)
        }
    });
};

module.exports.updateUsrPswrdPrcs1 = function(model, callback) {
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        if (r1.rowLength > 0) {
            callback(null, r1.rows[0]);
        } else {
            callback(['usrNtExst'], null);
        }
    });
};

module.exports.updateUsrPswrdPrcs2 = function(model, callback) {
    model.encPhn = bcrypt.hashSync(model.phn, JSON.parse(env.PK_SALT)[_.last(model.phn)]);
    model.encPswrd = bcrypt.hashSync(model.pswrd);
    e(q.usrByPhn.get, model, callback, function(d, r1) {
        var usrByPhn = r1.rows[0];
        if (r1.rowLength > 0 && usrByPhn.psCd == model.psCd) {
            e(q.usrByPhn.updateEncPswrd, model, callback, function() {
                callback(null, {});
            });
        } else {
            callback(['psCdWrng'], null);
        }
    });
};

module.exports.getCrcl = function(model, callback) {

    e(q.usr.get, model, callback, function(d, r) {
        if (r.rowLength > 0) {
            model.usrnm = r.rows[0].usrnm;
            e(q.sbscrb.getUsrMV, model, callback, function(d1, r1) {
                if (r1.rowLength > 0) {
                    model.rtUsrnm = r1.rows[0].rtUsrnm;
                    e(q.usr.getUsrCrcl, { rtUsrnm: model.rtUsrnm }, callback, function(d2, r2) {
                        console.log(JSON.stringify(r2))
                        console.log(JSON.stringify(d2))
                        callback(null, r2.rows);
                    });
                }

            });
        } else {
            callback(['usrIdWrong'], null);
        }
    });

};

