'use strict';
var env = process.env;
var log = process.log;

var async = require('async');
var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var q = db.queries;
var e = db.execute;

module.exports.addSbscrb = function(model, callback) {
    async.parallel([
        function(aCb) {
        e(q.sbscrb.get, model, callback, function getSbscrbCB(d, r) {
                if (r.rowLength > 0)
                    aCb({}, null);
                else
                    aCb(null, {});
            });
        },
        function(aCb) {
            //get root prmcd
            console.log("model.prmCd"+model.prmCd);
           e(q.sbscrb.getUsrMV, {usrnm:model.prmCd}, callback, function getrtUsrnmCBMV(d1, r1) {
               console.log("r11"+JSON.stringify(r1))
                if (r1.rowLength > 0){
                    console.log("inside"+JSON.stringify(r1))
                    model.rtUsrnm= r1.rows[0].rtUsrnm;
                    aCb(null, {});
                } else {
                     model.rtUsrnm = model.usrnm;
                      aCb(null, {});
                }
                    
             
                   
            });
        },
        function(aCb) {
            e(q.sbscrb.getUsrMV, model, callback, function getSbscCBUsrMV(d, r) {
                if (r.rowLength > 0)
                    aCb({}, null);
                else
                    aCb(null, {});
            });
        }
    ], function(e, r) {
        if (e)
            callback(['usrnmExst'], null);
        else{
            model.id=  null, model.flNm = '', model.rnk = '5', model.arms = '0' , model.pblcKy = '', model.rqstSnt = '',model.prflPC ='';
            db.executeBatch([
                {
                    query: q.sbscrb.add,
                    params: model
                },
                {
                    query: q.prm.add,
                    params: model
                }
               
            ], callback);
        }
    });
};


module.exports.getSbscrbEml = function(model, callback) {
    e(q.sbscrb.get, model, callback, function getSbscUsrEmlCB(dummy, res) {
        if (res.rowLength > 0) {
            callback(null, res.rows[0]);
        } else {
            callback(null, {});
        }
    });
};