'use strict';
var env = process.env;
var log = process.log;

var crypto = require('crypto');
var restify = require('restify');
var cookieHelper = require('cookie');
var _ = require('lodash');

function sha1(str) {
    return crypto.createHash('sha1').update(str).digest('hex');
}

function sign(val, secret) {
    return crypto
        .createHmac('sha256', secret)
        .update(val)
        .digest('hex');
        //.replace(/\=+$/, '');
};

module.exports = {
    preParser: function preParser() {
        return function customAuthorizationPreParser(req, res, next) {

            var allowed = [
                { m: 'get', u: '/api/sbscrb/eml' },
                { m: 'post', u: '/api/sbscrb' },
                { m: 'post', u: '/api/usr/prcs/1' },
                { m: 'post', u: '/api/usr' },
                { m: 'put', u: '/api//usr/phn/prcs/1' },
                { m: 'put', u: '/api/usr/pswrd/prcs/1' }
            ];

            var r = { m: req.method.toLowerCase(), u: req.url.match('^[^?]*')[0] };

            if (
                !_.startsWith(req.url, '/api/') ||
                (_.findIndex(allowed, r) > -1)
            ) {
                return next();
            }

            var strCookie = _.trim(req.headers["cookie"]);

            if (_.isEmpty(strCookie)) {
                return next(new restify.InvalidHeaderError('Authorization details required.'));
            }

            var objCookie = cookieHelper.parse(strCookie);

            if (!objCookie.a) {
                return next(new restify.InvalidHeaderError('Authorization details required.'));
            }

            var arrSignature = _.split(objCookie.a, '.', 2);

            if (!(arrSignature && arrSignature.length == 2)) {
                return next(new restify.InvalidHeaderError('Authorization details required.'));
            }

            var token = arrSignature[0],
                signedInputText = arrSignature[1],
                signedNewText = sign(token, token + env.AUTH_SECRET)
                ;

            if (sha1(signedInputText) != sha1(signedNewText)) {
                return next(new restify.InvalidHeaderError('Authorization details required.'));
            }

            req.authorization = {
                token: token
            };

            return next();
        };
    },
    postParser: function postParser() {
        return function customAuthorizationPostParser(req, res, next) {
            if(res.body && res.body.token){
                var token = res.body.token,
                    cookieOptions = {
                        domain: env.SERVER_DOMAIN,
                        path: '/',
                        secure: (env.NODE_ENV === 'production'),
                        httpOnly: true,
                        firstPartyOnly: true,
                        maxAge: _.toInteger(env.AUTH_EXPIRES)
                    },
                    signedNewText = sign(token, token + env.AUTH_SECRET);
                res.setHeader('Set-Cookie', cookieHelper.serialize('a', token + '.' + signedNewText, cookieOptions));
            }
            
            res.body = _.omit(res.body,['token']);
            res.send(res.body);

            return next();
        }
    }
};

