'use strict'

module.exports = function() {
    var crypto = require("crypto"),
        hex = 'hex',
        utf8 = 'utf8';
    var util = {
        pblcEnc: function(publicKey, text) {
            return crypto.publicEncrypt(publicKey, new Buffer(text)).toString(hex);
        },
        prvtDc: function(privateKey, text) {
            return crypto.privateDecrypt(privateKey, new Buffer(text, hex)).toString();
        },
        aesEnc: function(password, text) {
            var cipher = crypto.createCipher('aes192', password),
                encrypted = cipher.update(text, utf8, hex);
            return encrypted += cipher.final(hex);
        },
        aesDc: function(password, text) {
            var decipher = crypto.createDecipher('aes192', password),
                decripted = decipher.update(text, hex, utf8)
            return decripted += decipher.final(utf8);
        },
        md5: function(text){
            return crypto.createHash("md5").update(text).digest(hex);
        },
        gnKy: function(){
            return crypto.randomBytes(128).toString(hex);
        }
    };
    return util;
} ();