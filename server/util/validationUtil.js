'use strict'

module.exports = function() {
    var util = function(route, model, validCB, cb) {
        var api = "/api",
            usr = api + '/usr',
            sbscrb = api + '/sbscrb',
            prcs = '/prcs',
            rqst = api + '/rqst',
            emlRoutes = [sbscrb + '/eml', sbscrb, usr + prcs + '/1'],
            phnRoutes = [usr + prcs + '/1', usr, usr + '/pswrd' + prcs + '/1', usr +'/phn'+ prcs +'/1'],
            pswrdRoutes = [usr + prcs + '/1', usr, usr + '/pswrd' + prcs + '/2', usr +'/phn'+ prcs +'/1'],
            psCdRoutes = [usr + prcs + '/2', usr + '/pswrd' + prcs + '/2', usr +'/phn'+ prcs +'/2'],
            flNmRoutes = [usr + prcs + '/1'],
            usrnmRoutes = [sbscrb],
            nwPhnRoutes = [usr +'/phn'+ prcs +'/1'],
            nwPsCdRoutes = [usr +'/phn'+ prcs +'/2'],
            usrIdRoutes = [rqst + '/c'],
            crclRoutes = [rqst + '/ca'],
            rqstRoutes = [rqst],
            err = [];
        
        sanitize(model);
           console.log("route"+JSON.stringify(model))
        if (emlRoutes.indexOf(route) > -1 && !(model.eml && /^([\w!.%+\-])+@([\w\-])+(?:\.[\w\-]+)+$/.test(model.eml)))
            err.push('eml');
        
        if (phnRoutes.indexOf(route) > -1 && !(model.phn && model.cntryCd) && /^([\d]{10,15})$/.test(model.cntryCd + model.phn) && /^([\d]{1,3})$/.test(model.cntryCd))
            err.push('phn');
        
        if (pswrdRoutes.indexOf(route) > -1 && !(model.pswrd && /^(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*_\-])([\w!@#$%^&*_\-]{8,25})$/.test(model.pswrd)))
            err.push('pswrd');
        
        if (psCdRoutes.indexOf(route) > -1 && !(model.psCd && /^([\d]{4})$/.test(model.psCd)))
            err.push('psCd');
        
        if (flNmRoutes.indexOf(route) > -1 && !(model.flNm && /^([\w\s]{4,25})$/.test(model.flNm)))
            err.push('flNm');
        
        if (usrnmRoutes.indexOf(route) > -1 && !(model.usrnm && /^([a-z])+([\w_])*$/.test(model.usrnm)))
            err.push('usrnm');
        
        if (nwPhnRoutes.indexOf(route) > -1 && !(model.nwCntryCd && model.nwPhn && /^([\d]{10,15})$/.test(model.nwCntryCd + model.nwPhn) && /^([\d]{1,3})$/.test(model.nwCntryCd)))
            err.push('nwPhn');
        
        if (nwPsCdRoutes.indexOf(route) > -1 && !(model.nwPsCd && /^([\d]{4})$/.test(model.nwPsCd)))
            err.push('nwPsCd');
          
           
            
      
           console.log("err"+err.length)
        
        
        err.length > 0 ? cb(err, null) : validCB(model, cb);
    };

    var sanitize = function(model){
        for (var p in model) {
            if(model[p])
                model[p] = model[p].trim();
        }
        if(model.eml)
            model.eml = model.eml.toLowerCase();
        if(model.usrnm)
            model.usrnm = model.usrnm.toLowerCase();
        if(model.flNm)
            model.flNm = model.flNm;
        if(model.phn)
            model.phn = model.phn.replace(/[().\s-]/g,'');
        if(model.nwPhn)
            model.nwPhn = model.phn.replace(/[().\s-]/g,'');
    };

    return util;
} ();