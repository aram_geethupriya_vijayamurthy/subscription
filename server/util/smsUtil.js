var env = process.env;
var log = process.log;

var sendPasscode=function(phnNo,passcode,smsCB){
    var plivo = require('plivo');
    var p = plivo.RestAPI({
         authId: env.AUTH_ID,
         authToken: env.AUTH_TOKEN
});
   console.log("passcode"+passcode)
   console.log("phnNo"+phnNo)
 var params = {
    'src': env.SRC_PHN_NO, 
    'dst' : phnNo, 
    'text' : "This is "+ passcode, 
    'method' : "GET" 
};

// Prints the complete response
p.send_message(params, function (status, response) {
    console.log('Status: ', status);
    console.log('API Response:\n', response);
    console.log('Message UUID:\n', response['message_uuid']);
    console.log('Api ID:\n', response['api_id']);
     smsCB(status);
});
}

module.exports = sendPasscode