'use strict'
require('dotenv').config({ path: './server/.env' });
var env = process.env;
var ln = require("ln");
var log = process.log = new ln({
    "name": env.LOG_BATCH_CREATEKEY_NAME,
    "level": env.LOG_BATCH_CREATEKEY_LEVEL,
    "appenders": [{
        "type": "file",
        "path": env.LOG_BATCH_CREATEKEY_PATH
    }]
});
var async = require('async');
var _ = require('lodash');
var cp = require('child_process');

var db = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'dal' + env.SLASH + 'helper' + env.SLASH + 'databaseHelper');
var createKeysCB = function() {
    return function addSbscKeyCB(e, r) {
        if (e) {
            log.e("error");
        } else {
            log.info('key generated successfully for this usr' + r);
            db.execute(db.queries.user.updateSbscKey, r, function updateSbscKeyCB(dummy, res1) {
                log.info("done" + JSON.stringify(dummy));
            });


        }
    }
};
//as soon as que push
var q = async.queue(function(eml, callback) {
     var pvtKyCP = cp.exec('openssl genrsa 3072', function(e, stdout, stderr) {
        if (e) {
            log.error(e)
            callback(e, null);
        } else {
            var prvtKy = stdout, pbKy,
            pbKyCP = cp.spawn('openssl', ['rsa', '-pubout'])
            pbKyCP.stdout.setEncoding('ascii');
            pbKyCP.stdin.write(prvtKy);
            pbKyCP.stdin.end();
            pbKyCP.stdout.on('data', function(data) {
                pbKy = data;
                var model = {
                    eml: eml,
                    prvtKy: prvtKy,
                    pblcKy: data,
                    kyFlg: true
                }

                callback(null, model);
            });
       }
    });
}, env.CREATE_KEY_COUNT);

q.drain = function() {
    console.log('all items have been processed');
}

//first

db.executeRow(db.queries.user.getEmails, {}, function emailCallback(dummy, row) {
    log.info("res" + JSON.stringify(row));
    var eml = row.eml;
    q.push(eml, createKeysCB());
}, function(err, result) {
    if(err) log.e(err);
    else 
    log.info("All emails processed"+ Date().now());
    
}

);
