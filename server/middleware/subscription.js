'use strict';
var env = process.env;
var log = process.log;

var subscribeFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'subscriptionFacade');
var routes = [];

routes.push({
    meta: {
        name: 'getEmail',
        method: 'GET',
        paths: [
            '/sbsc/eml'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getEmail(req, res, next) {
                console.log("getEml")
          var model = {
               eml:req.params.eml
            
        };
        return subscribeFacade.getEmail(model, function getEmailCB(err, result) {
            if(err){
                return next(err);
            }else{
                
                res.body = result;
                return next();
            }
        });
    }
});

routes.push({
    meta: {
        name: 'addSbsc',
        method: 'POST',
        paths: [
            '/sbsc'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addSbsc(req, res, next) {
        console.log("addSbsc"+req.params.usrnm.toLowerCase())
          var model = {
            usrnm: req.params.usrnm.toLowerCase(),
            prmCd:req.params.prmCd.toLowerCase(),
            eml:req.params.eml
            
        };
        
        return subscribeFacade.addSbsc(model, function addSbscCB(err, result) {
            
            if(err){
              return next(err);
            }else{
              res.body = result;
              return next();
            }
        });
    }
});




module.exports = routes;