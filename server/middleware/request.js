'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var _ = require('lodash');
var requestFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'requestFacade');
var validate = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

function genericHandler(res, next) {
    return function(err, result) {
        if (err) {
            if (_.isArray(err))
                err = new restify.BadRequestError(JSON.stringify(err));
            return next(err);
        } else {
            res.body = result;
            return next();
        }
    };
};

var routes = [];
routes.push({
    meta: {
        name: 'addRqstPrcs1',
        method: 'POST',
        paths: [
           '/rqst/c'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addRqstPrcs1(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
    
        var model = {
          
            frndId: req.params.frndId,
            dtlsJSON: req.params.dtlsJSON,
            usrId: usrId,
            rqstId: req.params.rqstId
           
        };

        return validate(req.url.match('^[^?]*')[0], model, requestFacade.addRqstPrcs1, genericHandler(res, next));

    }
});



routes.push({
    meta: {
        name: 'updateRqstPrcs2',
        method: 'PUT',
        paths: [
           '/rqst/c'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function updateRqstPrcs2(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
         console.log("usrId"+usrId)
        var model = {
          
            id: req.params.id,           
            usrId: usrId,
            dtlsJSON: req.params.dtlsJSON
           


        };

         return validate(req.url.match('^[^?]*')[0], model, requestFacade.updateRqstPrcs2, genericHandler(res, next));

    }
});

routes.push({
    meta: {
        name: 'addAllRqstPrcs1',
        method: 'POST',
        paths: [
           '/rqst/ca'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addRqstPrcs1(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
    
        var model = {
          id: usrId
        };
        console.log("middle")

        return validate(req.url.match('^[^?]*')[0], model, requestFacade.addAllRqstPrcs1, genericHandler(res, next));

    }
});

routes.push({
    meta: {
        name: 'getRqst',
        method: 'GET',
        paths: [
           '/rqst'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getRqst(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
    
        var model = {
          usrId: usrId,
          typ:req.params.typ
        };
        console.log("middle")

        return validate(req.url.match('^[^?]*')[0], model, requestFacade.getRqst, genericHandler(res, next));

    }
});

routes.push({
    meta: {
        name: 'addArmRqstPrcs1',
        method: 'POST',
        paths: [
           '/rqst/a'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addArmRqstPrcs1(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
    
        var model = {
                     
            dtlsJSON: req.params.dtlsJSON,
            usrId: usrId,
      
           
        };

        return validate(req.url.match('^[^?]*')[0], model, requestFacade.addArmRqstPrcs1, genericHandler(res, next));

    }
});

module.exports = routes;