'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var _ = require('lodash');
var userFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'userFacade');
var validate = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

function genericHandler(res, next) {
    return function(err, result) {
        if (err) {
            if (_.isArray(err))
                err = new restify.BadRequestError(JSON.stringify(err));
            return next(err);
        } else {
            res.body = result;
            return next();
        }
    };
};

var routes = [];
routes.push({
    meta: {
        name: 'addUsrPrcs1',
        method: 'POST',
        paths: [
            '/usr/prcs/1'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addUsrPrcs1(req, res, next) {
        var model = {
            cntryCd: req.params.cntryCd,
            phn: req.params.phn,
            pswrd: req.params.pswrd,
            eml: req.params.eml,
            flNm: req.params.flNm
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.addUsrPrcs1, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'addUsrPrcs2',
        method: 'POST',
        paths: [
            '/usr/prcs/2'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addUsrPrcs2(req, res, next) {
        var model = {
            phn: req.authorization.token,
            psCd: req.params.psCd
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.addUsrPrcs2, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'getUsr',
        method: 'POST',
        paths: [
            '/usr'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getUsr(req, res, next) {
        var model = {
            cntryCd: req.params.cntryCd,
            phn: req.params.phn,
            pswrd: req.params.pswrd
        };
        console.log("middel")
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.getUsr, genericHandler(res, next));
    }
    });
routes.push({
    meta: {
        name: 'removeUsr',
        method: 'DEL',
        paths: [
            '/usr'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function removeUsr(req, res, next) {
        var cookieOptions = {
                        domain: env.SERVER_DOMAIN,
                        path: '/',
                        secure: (env.NODE_ENV === 'production'),
                        httpOnly: true,
                        firstPartyOnly: true,
                        expires: new Date(1)
                    };
        res.setHeader('Set-Cookie', cookieHelper.serialize('a', '', cookieOptions));
        res.send();
        return next();
    }
});

routes.push({
    meta: {
        name: 'updateUsrPhnPrcs1',
        method: 'PUT',
        paths: [
            '/usr/phn/prcs/1'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function updateUsrPhnPrcs1(req, res, next) {
        var model = {
            cntryCd: req.params.cntryCd,
            phn: req.params.phn,
            nwCntryCd: req.params.nwCntryCd,
            nwPhn: req.params.nwPhn,
            pswrd: req.params.pswrd
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.updateUsrPhnPrcs1, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'updateUsrPhnPrcs2',
        method: 'PUT',
        paths: [
            '/usr/phn/prcs/2'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function updateUsrPhnPrcs2(req, res, next) {
        var model = {
            phn: req.authorization.token,
            psCd: req.params.psCd,
            nwPsCd: req.params.nwPsCd
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.updateUsrPhnPrcs2, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'updateUsrPswrdPrcs1',
        method: 'PUT',
        paths: [
            '/usr/pswrd/prcs/1'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function updateUsrPswrdPrcs1(req, res, next) {
        var model = {
            cntryCd: req.params.cntryCd,
            phn: req.params.phn
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.updateUsrPswrdPrcs1, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'updateUsrPswrdPrcs2',
        method: 'PUT',
        paths: [
            '/usr/pswrd/prcs/2'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function updateUsrPswrdPrcs2(req, res, next) {
        var model = {
            phn: req.authorization.token,
            psCd: req.params.psCd,
            pswrd: req.params.pswrd
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.updateUsrPswrdPrcs2, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'getCntnCrcl',
        method: 'GET',
        paths: [
            '/usr/crcl'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getCrcl(req, res, next) {
          var usrId =  req.authorization.token;
    
        var model = {
          id: usrId
        };
        
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.getCrcl, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'getUsrNtfctn',
        method: 'GET',
        paths: [
            '/usr/ntfctn'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getUsrNtfctn(req, res, next) {
        var model = {
            usrId: req.authorization.token,
        };
        
        return userFacade.getUsrNtfctn(model, genericHandler(res, next));
    }
});

module.exports.getUsrNtfctn = function(model, callback){
    e(q.ntfctn.get, model, callback, function(d, r){
        callback(null, r.rows);
    });
};

module.exports = routes;