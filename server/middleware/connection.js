'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var _ = require('lodash');
var connectionFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'connectionFacade');
var validate = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

function genericHandler(res, next) {
    return function(err, result) {
        if (err) {
            if (_.isArray(err))
                err = new restify.BadRequestError(JSON.stringify(err));
            return next(err);
        } else {
            res.body = result;
            return next();
        }
    };
};

var routes = [];

routes.push({
    meta: {
        name: 'getCntn',
        method: 'GET',
        paths: [
           '/cntn'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getCntn(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
         console.log("usrId"+usrId)
        var model = {
          
            usrId: usrId
           


        };

         return validate(req.url.match('^[^?]*')[0], model, connectionFacade.getCntn, genericHandler(res, next));

    }
});

routes.push({
    meta: {
        name: 'removeCntn',
        method: 'DEL',
        paths: [
           '/cntn'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function removeCntn(req, res, next) {
        //var auth = req.authorization[req.authorization.scheme];
       var usrId =  req.authorization.token;
         console.log("usrId"+usrId)
        var model = {
          
            usrId: usrId,
            frndId: req.params.frndId
           


        };

         return validate(req.url.match('^[^?]*')[0], model, connectionFacade.removeCntn, genericHandler(res, next));

    }
});
module.exports = routes;