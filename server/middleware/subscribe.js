'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var _ = require('lodash');
var subscribeFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'subscribeFacade');
var validate = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

function genericHandler(res, next) {
    return function(err, result) {
        if (err) {
            if (_.isArray(err))
                err = new restify.BadRequestError(JSON.stringify(err));
            return next(err);
        } else {
            res.body = result;
            return next();
        }
    };
};

var routes = [];
routes.push({
    meta: {
        name: 'getSbscrbEml',
        method: 'GET',
        paths: [
            '/sbscrb/eml'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function getSbscrbEml(req, res, next) {
        var model = {
            eml: req.params.eml
        };
        
        return validate(req.url.match('^[^?]*')[0], model, subscribeFacade.getSbscrbEml, genericHandler(res, next));
    }
});

routes.push({
    meta: {
        name: 'addSbscrb',
        method: 'POST',
        paths: [
            '/sbscrb'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addSbscrb(req, res, next) {
        var model = {
            lcl: req.params.lcl,
            usrnm: req.params.usrnm,
            eml: req.params.eml,
            prmCd: req.params.prmCd
        };
        return validate(req.url.match('^[^?]*')[0], model, subscribeFacade.addSbscrb, genericHandler(res, next));
    }
});

module.exports = routes;