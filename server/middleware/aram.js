'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');
var _ = require('lodash');
var userFacade = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'bl' + env.SLASH + 'userFacade');
var validate = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'util' + env.SLASH + 'validationUtil');

function genericHandler(res, next) {
    return function(err, result) {
        if (err) {
            if (_.isArray(err))
                err = new restify.BadRequestError(JSON.stringify(err));
            return next(err);
        } else {
            res.body = result;
            return next();
        }
    };
};

var routes = [];
routes.push({
    meta: {
        name: 'addUsrPrcs1',
        method: 'POST',
        paths: [
            '/usr/prcs/1'
        ],
        version: env.SERVER_VERSION
    },
    middleware: function addUsrPrcs1(req, res, next) {
        var model = {
            cntryCd: req.params.cntryCd,
            phn: req.params.phn,
            pswrd: req.params.pswrd,
            eml: req.params.eml,
            flNm: req.params.flNm
        };
        
        return validate(req.url.match('^[^?]*')[0], model, userFacade.addUsrPrcs1, genericHandler(res, next));
    }
});

module.exports = routes;