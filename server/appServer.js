'use strict';
var env = process.env;
var log = process.log;

var restify = require('restify');

var serverOptions = {
    name: env.SERVER_NAME,
    version: env.SERVER_VERSION,
    acceptable: env.SERVER_ACCEPTABLE
};

if (env.NODE_ENV === 'production') {
    var fs = require('fs');
    serverOptions.key = fs.readFileSync(env.SECURITY_CERT_KEY);
    serverOptions.certificate = fs.readFileSync(env.SECURITY_CERT);
}

var server = restify.createServer(serverOptions);
var authParser = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'plugins' + env.SLASH + 'customAuthorizationParser');

var plugins = [
    function httpsVerifier(req, res, next) {
        if (env.NODE_ENV === 'production' && (!req.isSecure() || req.headers['x-forwarded-proto'] != 'https')) {
            res.redirect({
                hostname: env.SERVER_HOST,
                pathname: req.url,
                secure: true,
                permanent: true,
                query: req.params
                }, next);
        }
        return next();
    },
    restify.acceptParser(server.acceptable),
    restify.dateParser(),
    restify.queryParser(),
    restify.gzipResponse(),
    restify.bodyParser(),
    restify.requestExpiry({
        header: 'xrequestexpirytime'
    }),
    restify.throttle(JSON.parse(env.SERVER_THROTTLE)),
    //restify.CORS(),
    //add custom plugin for eTags
    authParser.preParser()
];

server.use(plugins);

require('./appRoutes')(server);

server.on('after',authParser.postParser());

server.get(/\/?.*/, restify.serveStatic({
    directory: './client/build',
    default: 'index.html'
}));

module.exports = server;