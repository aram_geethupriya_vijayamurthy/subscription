'use strict';
var env = process.env;
var log = process.log;

var fs = require('fs');
var async = require('async');
var cassandra = require('cassandra-driver');
var _ = require('lodash');

module.exports = {
    prepareDB: function prepareDB(callback) {
        var options = {
            contactPoints: env.DB_CONTACT_POINTS.split(',')
        };
        if (!_.isEmpty(env.DB_PORT)) {
            options.protocolOptions = {
                port: env.DB_PORT
            };
        }
        if (!_.isEmpty(env.DB_USERNAME)) {
            options.authProvider = new cassandra.auth.PlainTextAuthProvider(env.DB_USERNAME, env.DB_PASSWORD);
        }

        var client = new cassandra.Client(options);

        client.on('log', function onDBClientLog(level, className, message, furtherInfo) {
            if (level == 'info' || level == 'verbose')
                log.info('log event: %s -- %s', level, message);
            if (level == 'warning')
                log.warn('log event: %s -- %s', level, message);
            if (level == 'error')
                log.error('log event: %s -- %s', level, message);
        });

        var strDDL = fs.readFileSync(env.ROOT + env.SLASH + 'server' + env.SLASH + 'ddl.cql');
        strDDL = _.replace(strDDL,/(\r\n|\n|\r)/gm,'');
        strDDL = _.replace(strDDL,/\s\s+/g,' ');
        strDDL = _.trimEnd(strDDL, ';');
        
        var arrDDL = _.split(strDDL, ';');
        
        async.eachSeries(arrDDL, function runDDL(q, aCbk) {
            client.execute(q, function(e, r) {
                if (e)
                    aCbk(e, null);
                else
                    aCbk(null, {});
            });
        }, callback);
    }
};
