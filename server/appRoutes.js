'use strict';
var env = process.env;
var log = process.log;

var Routes = function(server) {
    var registerRoute = function registerRoute(route) {

        var routeMethod = route.meta.method.toLowerCase();
        var routeName = route.meta.name;
        var routeVersion = route.meta.version;

        route
            .meta
            .paths
            .forEach(function(aPath) {
                var routeMeta = {
                    name: routeName,
                    path: "/api" + aPath,
                    version: routeVersion
                };
                server[routeMethod](routeMeta, route.middleware);
            });

    };

    var setupMiddleware = function setupMiddleware(middlewareName) {
        var routes = require(env.ROOT + env.SLASH + 'server' + env.SLASH + 'middleware' + env.SLASH + middlewareName);
        routes.forEach(registerRoute);
    };

    [
        'subscribe',
        'user',
        'request',
        'connection'
        // ... more middleware ... //
    ]
        .forEach(setupMiddleware);
};

module.exports = Routes;